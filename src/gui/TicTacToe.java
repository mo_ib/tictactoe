package gui;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Board;
import model.Combo;
import model.Game;

import java.security.cert.PolicyNode;
import java.util.ArrayList;
import java.util.List;

public class TicTacToe extends Application {
    private Game game = new Game();
    private Pane root = new Pane();
    Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Tic Tac Toe");
        window.setScene(new Scene(creatMainMenu()));

        //if
        //primaryStage.setScene(new Scene(game.getBoard().generateBoard()));
        window.show();
    }

    private Parent creatMainMenu() {
        root.setPrefSize(600,600);
        Button newGameHuman = new Button("Human  VS  Human");
        newGameHuman.setTranslateX(220);
        newGameHuman.setTranslateY(200);

        Button newGameAi = new Button("Human  VS  AI");
        newGameAi.setTranslateX(235);
        newGameAi.setTranslateY(250);

        Button exit = new Button("Exit");
        exit.setTranslateX(265);
        exit.setTranslateY(300);

        root.getChildren().addAll(newGameHuman,newGameAi,exit);

        newGameHuman.setOnAction(event -> {
            window.setScene(new Scene(game.getBoard().generateBoard()));
        });

        exit.setOnAction(event -> {
            window.close();
        });
        return root;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void setRoot(Pane root){
        this.root=root;
    }
}
