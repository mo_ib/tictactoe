package model;


import javafx.geometry.Pos;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Tile extends StackPane {
    private Text text = new Text();
    private Boolean turn;

    public Tile() {

        Rectangle border = new Rectangle(200, 200);
        border.setFill(null);
        border.setStroke(Color.BLACK);
        setAlignment(Pos.CENTER);
        getChildren().addAll(border, text);

        text.setFont(Font.font(72));


        mouseClick();
    }

    private void mouseClick() {
        setOnMouseClicked(event -> {
            if (!Game.playable) {
                return;
            }

            if (event.getButton() == MouseButton.PRIMARY) {
                if (text.getText().isEmpty()) {
                    if (!Game.turnX) {
                        drawO();
                        Game.turnX = (true);
                        Game.checkState();
                    } else if (Game.turnX) {
                        drawX();
                        Game.turnX = (false);
                        Game.checkState();
                    }
                }
            }
        });
    }

    private void drawX() {
        text.setText("X");
    }

    private void drawO() {
        text.setText("O");
    }

    public void setText(String text){
        this.text.setText(text);
    }

    public String getValue(){
        return text.getText();
    }

    public double getCenterX() {
        return getTranslateX()+100;
    }

    public double getCenterY() {
        return getTranslateY()+100;
    }
}
