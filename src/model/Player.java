package model;

import javafx.scene.text.Text;

public abstract class Player {

    public enum PlayerType{
        HUMAN, AI
    }

    private PlayerType playerType;
    private Text playerText;

    public Player(PlayerType playerType, Text playerText) {
        this.playerType = playerType;
        this.playerText = playerText;
    }
}
