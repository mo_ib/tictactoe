package model;

import javafx.scene.Parent;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;

public class Board {
    private List<Player> players;
    private Pane root = new Pane();
    private Tile[][] board = new Tile[3][3];
    private List<Combo> combos = new ArrayList<>();

    public Board() {
        generateBoard();
    }

    public Parent generateBoard() {
        root.setPrefSize(600,600);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Tile tile = new Tile();
                tile.setTranslateX(j * 200);
                tile.setTranslateY(i * 200);

                root.getChildren().add(tile);

                board[j][i] = tile;
            }
        }

        for (int y=0; y<3; y++){
            combos.add( new Combo(board[0][y], board[1][y], board[2][y]));
        }
        for (int x=0; x<3; x++){
            combos.add( new Combo(board[x][0], board[x][1], board[x][2]));
        }
        combos.add( new Combo(board[0][0], board[1][1], board[2][2]));
        combos.add( new Combo(board[2][0], board[1][1], board[0][2]));

        return root;
    }

    public List<Combo> getCombos() {
        return combos;
    }

    public Tile getTile(int x, int y) {
        return board[x][y];
    }
}
