package model;

import javafx.scene.control.Alert;
import javafx.scene.input.MouseButton;

public class Game {
    private static Board board = new Board();
    public static Boolean turnX=true;
    public static Boolean playable=true;

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public static void checkState(){
        for(Combo combo:board.getCombos()){
            if (combo.isComplete()){
                playable = false;
                winAlert(combo);
                break;
            }
        }
    }

    private static void winAlert(Combo combo){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, combo.getTile(0).getValue()+" is Winner!");
        alert.show();
    }
}
